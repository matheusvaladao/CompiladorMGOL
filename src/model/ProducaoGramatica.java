/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;

/**
 *
 * @author Matheus
 */
public class ProducaoGramatica {

    private HashMap<String, Object[]> producaoGramatica = new HashMap<>();

    public ProducaoGramatica() {

        Object[] v1 = {"P'", 1, "P'→P"};
        producaoGramatica.put("1", v1);
        Object[] v2 = {"P", 3, "P→inicio V A"};
        producaoGramatica.put("2", v2);
        Object[] v3 = {"V", 2, "V→varinicio LV"};
        producaoGramatica.put("3", v3);
        Object[] v4 = {"LV", 2, "LV→D LV"};
        producaoGramatica.put("4", v4);
        Object[] v5 = {"LV", 2, "LV→varfim;"};
        producaoGramatica.put("5", v5);
        Object[] v6 = {"D", 3, "D→id TIPO;"};
        producaoGramatica.put("6", v6);
        Object[] v7 = {"TIPO", 1, "TIPO→inteiro"};
        producaoGramatica.put("7", v7);
        Object[] v8 = {"TIPO", 1, "TIPO→real"};
        producaoGramatica.put("8", v8);
        Object[] v9 = {"TIPO", 1, "TIPO→lit"};
        producaoGramatica.put("9", v9);
        Object[] v10 = {"A", 2, "A→ES A"};
        producaoGramatica.put("10", v10);
        Object[] v11 = {"ES", 3, "ES→leia id;"};
        producaoGramatica.put("11", v11);
        Object[] v12 = {"ES", 3, "ES→escreva ARG;"};
        producaoGramatica.put("12", v12);
        Object[] v13 = {"ARG", 1, "ARG→literal"};
        producaoGramatica.put("13", v13);
        Object[] v14 = {"ARG", 1, "ARG→num"};
        producaoGramatica.put("14", v14);
        Object[] v15 = {"ARG", 1, "ARG→id"};
        producaoGramatica.put("15", v15);
        Object[] v16 = {"A", 2, "A→CMD A"};
        producaoGramatica.put("16", v16);
        Object[] v17 = {"CMD", 4, "CMD→id rcb LD;"};
        producaoGramatica.put("17", v17);
        Object[] v18 = {"LD", 3, "LD→OPRD opm OPRD"};
        producaoGramatica.put("18", v18);
        Object[] v19 = {"LD", 1, "LD→OPRD"};
        producaoGramatica.put("19", v19);
        Object[] v20 = {"OPRD", 1, "OPRD→id"};
        producaoGramatica.put("20", v20);
        Object[] v21 = {"OPRD", 1, "OPRD→num"};
        producaoGramatica.put("21", v21);
        Object[] v22 = {"A", 2, "A→COND A"};
        producaoGramatica.put("22", v22);
        Object[] v23 = {"COND", 2, "COND→CABEÇALHO CORPO"};
        producaoGramatica.put("23", v23);
        Object[] v24 = {"CABECALHO", 5, "CABEÇALHO→se (EXP_R) então"};
        producaoGramatica.put("24", v24);
        Object[] v25 = {"EXP_R", 3, "EXP_R→OPRD opr OPRD"};
        producaoGramatica.put("25", v25);
        Object[] v26 = {"CORPO", 2, "CORPO→ES CORPO"};
        producaoGramatica.put("26", v26);
        Object[] v27 = {"CORPO", 2, "CORPO→CMD CORPO"};
        producaoGramatica.put("27", v27);
        Object[] v28 = {"CORPO", 2, "CORPO→COND CORPO"};
        producaoGramatica.put("28", v28);
        Object[] v29 = {"CORPO", 1, "CORPO→fimse"};
        producaoGramatica.put("29", v29);
        Object[] v30 = {"A", 1, "A→fim"};
        producaoGramatica.put("30", v30);
    }

    /**
     * Metodo que retorna a quantidade de casas da producao que tera que ser
     * voltada.
     *
     * @param estado
     * @param naoTerminal
     * @return
     */
    public int obterQuantidadeCasas(String estado) {
        Object[] retorno = producaoGramatica.get(estado);
        return (int) retorno[1];
    }

    /**
     * Metodo que ira obter o produtor.
     *
     * @param estado
     * @return
     */
    public String obterProdutor(String estado) {
        Object[] retorno = producaoGramatica.get(estado);
        return (String) retorno[0];
    }

    /**
     * Metodo que obtem as producoes da gramatica.
     *
     * @param estado
     * @return
     */
    public String obterProducao(String estado) {
        Object[] retorno = producaoGramatica.get(estado);
        return (String) retorno[2];
    }

}
