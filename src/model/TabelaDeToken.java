/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matheus
 */
public class TabelaDeToken {

    private List<LinhaTabelaToken> coluna;

    public TabelaDeToken() {
        coluna = new LinkedList<LinhaTabelaToken>();
        LinhaTabelaToken linha0 = new LinhaTabelaToken(false, null);
        linha0.insereProximoEstado("D", "1");
        linha0.insereProximoEstado("L", "4");
        linha0.insereProximoEstado("\"", "3");
        linha0.insereProximoEstado("{", "20");
        linha0.insereProximoEstado("Tab", "0");
        linha0.insereProximoEstado("Salto", "0");
        linha0.insereProximoEstado("Esp", "0");
        linha0.insereProximoEstado(">", "6");
        linha0.insereProximoEstado("<", "8");
        linha0.insereProximoEstado("=", "10");
        linha0.insereProximoEstado("-", "11");
        linha0.insereProximoEstado("+", "11");
        linha0.insereProximoEstado("*", "11");
        linha0.insereProximoEstado("/", "11");
        linha0.insereProximoEstado("(", "18");
        linha0.insereProximoEstado(")", "12");
        linha0.insereProximoEstado(";", "17");
        linha0.insereProximoEstado("EOF", "19");

        LinhaTabelaToken linha1 = new LinhaTabelaToken(true, "num");
        linha1.insereProximoEstado("D", "1");
        linha1.insereProximoEstado("E", "14");
        linha1.insereProximoEstado("e", "14");
        linha1.insereProximoEstado(".", "2");

        LinhaTabelaToken linha2 = new LinhaTabelaToken(false, null);
        linha2.insereProximoEstado("D", "13");

        LinhaTabelaToken linha3 = new LinhaTabelaToken(false, null);
        linha3.insereProximoEstado(".", "3");
        linha3.insereProximoEstado("\"", "5");

        LinhaTabelaToken linha4 = new LinhaTabelaToken(true, "id");
        linha4.insereProximoEstado("D", "4");
        linha4.insereProximoEstado("L", "4");
        linha4.insereProximoEstado("_", "4");

        LinhaTabelaToken linha5 = new LinhaTabelaToken(true, "literal");
        LinhaTabelaToken linha6 = new LinhaTabelaToken(true, "opr");
        linha6.insereProximoEstado("=", "7");

        LinhaTabelaToken linha7 = new LinhaTabelaToken(true, "opr");
        LinhaTabelaToken linha8 = new LinhaTabelaToken(true, "opr");
        linha8.insereProximoEstado(">", "7");
        linha8.insereProximoEstado("=", "7");
        linha8.insereProximoEstado("-", "9");

        LinhaTabelaToken linha9 = new LinhaTabelaToken(true, "rcb");
        LinhaTabelaToken linha10 = new LinhaTabelaToken(true, "opr");
        LinhaTabelaToken linha11 = new LinhaTabelaToken(true, "opm");
        LinhaTabelaToken linha12 = new LinhaTabelaToken(true, "fc_p");
        LinhaTabelaToken linha13 = new LinhaTabelaToken(true, "num");
        linha13.insereProximoEstado("D", "13");
        linha13.insereProximoEstado("E", "14");
        linha13.insereProximoEstado("e", "14");

        LinhaTabelaToken linha14 = new LinhaTabelaToken(false, null);
        linha14.insereProximoEstado("D", "16");
        linha14.insereProximoEstado("-", "15");
        linha14.insereProximoEstado("+", "15");

        LinhaTabelaToken linha15 = new LinhaTabelaToken(false, null);
        linha15.insereProximoEstado("D", "16");

        LinhaTabelaToken linha16 = new LinhaTabelaToken(true, "num");
        LinhaTabelaToken linha17 = new LinhaTabelaToken(true, "pt_v");
        LinhaTabelaToken linha18 = new LinhaTabelaToken(true, "ab_p");
        LinhaTabelaToken linha19 = new LinhaTabelaToken(true, "EOF");
        LinhaTabelaToken linha20 = new LinhaTabelaToken(false, null);
        linha20.insereProximoEstado(".", "20");
        linha20.insereProximoEstado("}", "21");

        LinhaTabelaToken linha21 = new LinhaTabelaToken(true, "comentario");

        coluna.add(linha0);
        coluna.add(linha1);
        coluna.add(linha2);
        coluna.add(linha3);
        coluna.add(linha4);
        coluna.add(linha5);
        coluna.add(linha6);
        coluna.add(linha7);
        coluna.add(linha8);
        coluna.add(linha9);
        coluna.add(linha10);
        coluna.add(linha11);
        coluna.add(linha12);
        coluna.add(linha13);
        coluna.add(linha14);
        coluna.add(linha15);
        coluna.add(linha16);
        coluna.add(linha17);
        coluna.add(linha18);
        coluna.add(linha19);
        coluna.add(linha20);
        coluna.add(linha21);
    }

    /**
     * Metodo que obtem o proximo estado no AFD.
     *
     * @param estado
     * @param simbolo
     * @return
     */
    public int obterProximoEstado(int estado, String simbolo) {

        try {
            return coluna.get(estado).obterRroximoEstado(simbolo);
        } catch (Exception e) {
            return -1;
        }
    }

    /**
     * Metodo que verifica se o estado e final.
     *
     * @param estado
     * @return
     */
    public boolean eFinal(int estado) {
        return coluna.get(estado).getEFinal();
    }

    /**
     * Obtem token do estado atual.
     *
     * @param estado
     * @return
     */
    public String obterToken(int estado) {
        return coluna.get(estado).getToken();
    }

}
