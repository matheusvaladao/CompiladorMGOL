/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;

/**
 *
 * @author Matheus
 */
public class LinhaTabelaToken {

    private boolean eFinal;
    private String token;
    private HashMap<String, String> coluna;

    public LinhaTabelaToken(boolean eFinal, String token) {
        this.eFinal = eFinal;
        if (eFinal) {
            this.token = token;
        }

        coluna = new HashMap<String, String>();
        coluna.put("D", null);
        coluna.put("L", null);
        coluna.put("E", null);
        coluna.put("e", null);
        coluna.put("\"", null);
        coluna.put("{", null);
        coluna.put("}", null);
        coluna.put("_", null);
        coluna.put("Tab", null);
        coluna.put("Salto", null);
        coluna.put("Esp", null);
        coluna.put(">", null);
        coluna.put("<", null);
        coluna.put("=", null);
        coluna.put("-", null);
        coluna.put("+", null);
        coluna.put("*", null);
        coluna.put("/", null);
        coluna.put("(", null);
        coluna.put(")", null);
        coluna.put(";", null);
        coluna.put(".", null);
        coluna.put("EOF", null);
    }

    /**
     * Metodo que obtem o proximo estado a se pesquisar.
     *
     * @param key
     * @return
     */
    public int obterRroximoEstado(String key) {

        try {
            return Integer.parseInt(coluna.get(key));
        } catch (Exception e) {
            return -1;
        }
    }

    /**
     * Metodo que se alterar o valor do proximo estado.
     *
     * @param key
     * @param estado
     */
    public void insereProximoEstado(String key, String estado) {
        coluna.replace(key, estado);
    }

    public boolean getEFinal() {
        return eFinal;
    }

    public String getToken() {
        return token;
    }

}
