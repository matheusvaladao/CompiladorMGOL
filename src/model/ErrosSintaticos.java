/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;

/**
 *
 * @author Avilages
 */
public class ErrosSintaticos {

    HashMap<String, String> erroSintatico;

    /**
     * Metodo construtor da classe.
     */
    public ErrosSintaticos() {
        this.erroSintatico = new HashMap<>();
        erroSintatico.put("0", "O sistema esperava a palavra inicio.");
        erroSintatico.put("1", "O sistema esperava o fim do programa.");
        erroSintatico.put("2", "O sistema esperava a palavra varinicio.");
        erroSintatico.put("3", "O sistema esperava uma declaração de variavel ou a palavra varfim.");
        erroSintatico.put("4", "O sistema esperava o tipo de variável.");
        erroSintatico.put("5", "O sistema esperava um ;.");
        erroSintatico.put("6", "O sistema esperava um ;.");
        erroSintatico.put("7", "O sistema esperava um ;.");
        erroSintatico.put("8", "O sistema esperava um ;.");
        erroSintatico.put("9", "O sistema esperava um ;.");
        erroSintatico.put("10", "O sistema esperava uma declaração de variável ou a palavra varfim.");
        erroSintatico.put("11", "O sistema esperava uma declaração de variável ou a palavra varfim.");
        erroSintatico.put("12", "O sistema esperava uma declaração de variável ou a palavra varfim.");
        erroSintatico.put("13", "O sistema esperava uma declaração de variável ou a palavra varfim.");
        erroSintatico.put("14", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("15", "O sistema esperava uma variável.");
        erroSintatico.put("16", "O sistema esperava um;.");
        erroSintatico.put("17", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("18", "O sistema esperava um número, um literal, um ';' ou uma variável.");
        erroSintatico.put("19", "O sistema esperava um número, um literal, um ';' ou uma variável.");
        erroSintatico.put("20", "O sistema esperava um número, um literal, um ';' ou uma variável.");
        erroSintatico.put("21", "O sistema esperava um número, um literal, um ';' ou uma variável.");
        erroSintatico.put("22", "O sistema esperava um ;.");
        erroSintatico.put("23", "O sistema esperava um número, um literal, um ';' ou uma variável.");
        erroSintatico.put("24", "O sistema esperava uma atribuição.");
        erroSintatico.put("25", "O sistema esperava uma variável ou um número.");
        erroSintatico.put("26", "O sistema esperava uma variável ou um número.");
        erroSintatico.put("27", "O sistema esperava uma variável ou um número.");
        erroSintatico.put("28", "O sistema esperava um ;.");
        erroSintatico.put("29", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("30", "O sistema esperava uma variável ou um número.");
        erroSintatico.put("31", "O sistema esperava uma variável ou um número.");
        erroSintatico.put("32", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("33", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("34", "O sistema esperava um (.");
        erroSintatico.put("35", "O sistema esperava uma variável, um número, um ) ou um operador.");
        erroSintatico.put("36", "O sistema esperava um ).");
        erroSintatico.put("37", "O sistema esperava a palavra entao.");
        erroSintatico.put("38", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("39", "O sistema esperava um operador.");
        erroSintatico.put("40", "O sistema esperava uma variável ou um número.");
        erroSintatico.put("41", "O sistema esperava uma variável, um número, um ) ou um operador.");
        erroSintatico.put("42", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("43", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("44", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("45", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("46", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("47", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("48", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("49", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("50", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("51", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("52", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("53", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("54", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("55", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("56", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("57", "O sistema esperava um simbolo da linguagem ou variável.");
        erroSintatico.put("58", "O sistema esperava uma declaração de variável ou a palavra varfim.");

    }

    /**
     * Metodo que retorna o erro sintatico pelo codigo passado.
     *
     * @param codigoErro
     * @return
     */
    public String obterErro(String codigoErro) {
        return erroSintatico.get(codigoErro);
    }

}
