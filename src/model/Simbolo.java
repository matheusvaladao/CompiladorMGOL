/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Matheus
 */
public class Simbolo {

    private String lexico;
    private String token;
    private String tipo;

    public Simbolo(String lexico, String token, String tipo) {
        this.lexico = lexico;
        this.token = token;
        this.tipo = tipo;
    }

    public String getLexico() {
        return lexico;
    }

    public void setLexico(String lexico) {
        this.lexico = lexico;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Simbolo{Lexico: " + lexico + ", Token: " + token + ", Tipo: " + tipo + "}";
    }

}
