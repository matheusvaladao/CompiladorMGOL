/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author Matheus
 */
public class TabelaDeSintaxe {

    public HashMap<String, HashMap<String, String>> lerTabela(String caminhoArquivo) {

        HashMap<String, HashMap<String, String>> tabelaSintatica = new HashMap<>();
        BufferedReader br = null;
        String linha = "";
        String csvDivisor = ",";

        try {

            br = new BufferedReader(new FileReader(caminhoArquivo));

            String linhaCabecalho = br.readLine();
            String[] celulaCabecalho = linhaCabecalho.split(csvDivisor);

            while ((linha = br.readLine()) != null) {

                HashMap<String, String> linhaTabela = new HashMap<>();
                String[] celula = linha.split(csvDivisor);

                for (int i = 1; i < celula.length; i++) {
                    linhaTabela.put(celulaCabecalho[i], celula[i]);
                }
                tabelaSintatica.put(celula[0], linhaTabela);

            }

            return tabelaSintatica;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;

    }

}
