/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Set;
import model.Simbolo;

/**
 *
 * @author Matheus
 */
public class TabelaDeSimbolos {

    private HashMap<String, Simbolo> tabelaSimbolos = new HashMap<>();

    /**
     * Metodo que carrega a tabela de simbolos.
     */
    public TabelaDeSimbolos() {

        tabelaSimbolos.put("inicio", new Simbolo("inicio", "inicio", null));
        tabelaSimbolos.put("varinicio", new Simbolo("varinicio", "varinicio", null));
        tabelaSimbolos.put("varfim", new Simbolo("varfim", "varfim", null));
        tabelaSimbolos.put("escreva", new Simbolo("escreva", "escreva", null));
        tabelaSimbolos.put("leia", new Simbolo("leia", "leia", null));
        tabelaSimbolos.put("se", new Simbolo("se", "se", null));
        tabelaSimbolos.put("entao", new Simbolo("entao", "entao", null));
        tabelaSimbolos.put("senao", new Simbolo("senao", "senao", null));
        tabelaSimbolos.put("fimse", new Simbolo("fimse", "fimse", null));
        tabelaSimbolos.put("fim", new Simbolo("fim", "fim", null));
        tabelaSimbolos.put("int", new Simbolo("int", "int", null));
        tabelaSimbolos.put("lit", new Simbolo("lit", "lit", null));
        tabelaSimbolos.put("real", new Simbolo("real", "real", null));
    }

    /**
     * Metodo que obtem simbolo a partir de um lexema.
     *
     * @param lexema
     * @return
     */
    public Simbolo obterSimbolo(String lexema) {
        try {
            return tabelaSimbolos.get(lexema);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Metodo que insere um identificador na tabela de simbolos.
     *
     * @param chave
     * @param simbolo
     */
    public void inserirSimbolo(String chave, Simbolo simbolo) {
        tabelaSimbolos.put(chave, simbolo);
    }

    @Override
    public String toString() {
        String retorno = "";
        Set<String> chaves = tabelaSimbolos.keySet();
        for (String chave : chaves) {
            if (chave != null) {
                retorno = retorno + (chave + " - " + tabelaSimbolos.get(chave) + "\n");
            }
        }
        return retorno;
    }

}
