/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacao;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Stack;
import model.Simbolo;
import model.TabelaDeSimbolos;

/**
 *
 * @author Avilages
 */
public class AnalisadorSemantico {

    private FileWriter arquivo;
    private PrintWriter writer;
    private Stack<Simbolo> pilha;
    private String urlArquivo;
    private int contador;
    private String textoArquivo;
    private String cabecalhoArquivo;

    /**
     * Metodo construtor da classe. Cria arquivo final.
     *
     * @param path
     */
    public AnalisadorSemantico(String path) throws IOException {
        this.arquivo = new FileWriter(path);
        this.writer = new PrintWriter(arquivo);
        this.pilha = new Stack<>();
        this.contador = 0;
        textoArquivo = "";
        cabecalhoArquivo = "#include<stdio.h>\n";
        cabecalhoArquivo = cabecalhoArquivo + "typedef char literal[256];\n";
        cabecalhoArquivo = cabecalhoArquivo + "void main(void)\n";
        cabecalhoArquivo = cabecalhoArquivo + "{\n";
        cabecalhoArquivo = cabecalhoArquivo + "/*----Variaveis temporarias----*/\n";
    }

    /**
     * Metodo que adiciona simbolo na pilha semantica.
     *
     * @param simbolo
     */
    public void adicionarPilha(Simbolo simbolo) {
        pilha.push(simbolo);
    }

    /**
     * Metodo que faz a analise semantica do compilador.
     *
     * @param tabelaSimbolos
     * @param reducao
     *
     */
    public void analisar(TabelaDeSimbolos tabelaSimbolos, int reducao, int linha) throws IOException {

        switch (reducao) {

            case 1:
                //Nada.
                pilha.pop();
                pilha.push(new Simbolo("P`", null, null));
                break;
            case 2:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("P", null, null));
                break;
            case 3:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("V", null, null));
                break;
            case 4:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("LV", null, null));
                break;
            case 5:
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("LV", null, null));
                //Metodo imprimir ja impoe quebra.
                imprimir("");
                break;
            case 6:
                Simbolo pt_v = pilha.pop();
                Simbolo tipo = pilha.pop();
                Simbolo id = pilha.pop();
                pilha.push(new Simbolo("D", null, null));
                id.setTipo(tipo.getTipo());
                imprimir(tipo.getTipo() + " " + id.getLexico() + pt_v.getLexico());
                break;
            case 7:
                Simbolo inteiro = pilha.pop();
                pilha.push(new Simbolo("TIPO", null, pilha.peek().getTipo()));
                break;
            case 8:
                Simbolo real = pilha.pop();
                pilha.push(new Simbolo("TIPO", null, pilha.peek().getTipo()));
                break;
            case 9:
                Simbolo literal = pilha.pop();
                pilha.push(new Simbolo("TIPO", null, pilha.peek().getTipo()));
                break;
            case 10:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("A", null, null));
                break;
            case 11:
                Simbolo pt_v1 = pilha.pop();
                Simbolo id1 = pilha.pop();
                Simbolo leia = pilha.pop();
                pilha.push(new Simbolo("ES", null, null));
                if (id1.getTipo().equals(tabelaSimbolos.obterSimbolo(id1.getLexico()).getTipo())) {
                    if (id1.getTipo().equals("literal")) {
                        imprimir("scanf(\"%s\"," + id1.getLexico() + ");");
                    } else if (id1.getTipo().equals("int")) {
                        imprimir("scanf(\"%d\", &" + id1.getLexico() + ");");
                    } else if (id1.getTipo().equals("double")) {
                        imprimir("scanf(\"%lf\", &" + id1.getLexico() + ");");
                    }
                } else {
                    System.out.println("\n------------------------\nERRO SEMÂNTICO\nLinha: " + linha + " - Erro identificado na análise semântica.");
                    System.out.println("Erro: Variável não declarada.\n------------------------");
                    System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                    System.exit(0);
                }
                break;
            case 12:
                pilha.pop();
                Simbolo arg = pilha.pop();
                Simbolo escreva = pilha.pop();
                pilha.push(new Simbolo("ES", null, null));
                if (arg.getToken().equals("id")) {
                    if (arg.getTipo().equals("literal")) {
                        imprimir("printf(\"%s\"," + arg.getLexico() + ");");
                    } else if (arg.getTipo().equals("int")) {
                        imprimir("printf(\"%d\"," + arg.getLexico() + ");");
                    } else if (arg.getTipo().equals("double")) {
                        imprimir("printf(\"%lf\"," + arg.getLexico() + ");");
                    } else {
                        imprimir("printf(" + arg.getLexico() + ");");
                    }
                } else {
                    imprimir("printf(" + arg.getLexico() + ");");
                }

                break;
            case 13:
                Simbolo literal1 = pilha.pop();
                Simbolo arg1 = new Simbolo(literal1.getLexico(), literal1.getToken(), literal1.getTipo());
                pilha.push(arg1);
                break;
            case 14:
                Simbolo num = pilha.pop();
                Simbolo arg2 = new Simbolo(num.getLexico(), num.getToken(), num.getTipo());
                pilha.push(arg2);
                break;
            case 15:
                Simbolo id2 = pilha.pop();
                if (id2.getLexico().equals(tabelaSimbolos.obterSimbolo(id2.getLexico()).getLexico())) {
                    Simbolo arg3 = new Simbolo(id2.getLexico(), id2.getToken(), id2.getTipo());
                    pilha.push(arg3);
                } else {
                    System.out.println("\n------------------------\nERRO SEMÂNTICO\nLinha: " + linha + " - Erro identificado na análise semântica.");
                    System.out.println("Erro: Variável não declarada.\n------------------------");
                    System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                    System.exit(0);
                }
                break;
            case 16:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("A", null, null));
                break;
            case 17:
                Simbolo pt_v2 = pilha.pop();
                Simbolo ld = pilha.pop();
                Simbolo rcb = pilha.pop();
                Simbolo id3 = pilha.pop();
                pilha.push(new Simbolo("CMD", null, null));
                if (id3.getLexico().equals(tabelaSimbolos.obterSimbolo(id3.getLexico()).getLexico())) {
                    if (id3.getTipo().equals(ld.getTipo())) {
                        imprimir(id3.getLexico() + " " + rcb.getTipo() + " " + ld.getLexico() + ";");
                    } else {
                        System.out.println("\n------------------------\nERRO SEMÂNTICO\nLinha: " + linha + " - Erro identificado na análise semântica.");
                        System.out.println("Erro: Tipos diferentes para atribuição.\n------------------------");
                        System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                        System.exit(0);
                    }
                } else {
                    System.out.println("\n------------------------\nERRO SEMÂNTICO\nLinha: " + linha + " - Erro identificado na análise semântica.");
                    System.out.println("Erro: Variável não declarada.\n------------------------");
                    System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                    System.exit(0);
                }
                break;
            case 18:
                Simbolo oprdFinal = pilha.pop();
                Simbolo opm = pilha.pop();
                Simbolo oprdInicial = pilha.pop();
                if (oprdInicial.getTipo().equals(oprdFinal.getTipo())) {
                    Simbolo ld1 = new Simbolo("T" + contador, null, oprdFinal.getTipo());
                    cabecalhoArquivo = cabecalhoArquivo + oprdFinal.getTipo() + " T" + contador + ";\n";
                    contador++;
                    imprimir(ld1.getLexico() + " = " + oprdInicial.getLexico() + " " + opm.getTipo() + " " + oprdFinal.getLexico() + ";");
                    pilha.push(ld1);
                } else {
                    System.out.println("\n------------------------\nERRO SEMÂNTICO\nLinha: " + linha + " - Erro identificado na análise semântica.");
                    System.out.println("Erro: Operandos com tipos incompatíveis.\n------------------------");
                    System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                    System.exit(0);
                }
                break;
            case 19:
                Simbolo oprd = pilha.pop();
                Simbolo ld1 = new Simbolo(oprd.getLexico(), oprd.getToken(), oprd.getTipo());
                pilha.push(ld1);
                break;
            case 20:
                Simbolo id4 = pilha.pop();
                if (id4.getLexico().equals(tabelaSimbolos.obterSimbolo(id4.getLexico()).getLexico())) {
                    Simbolo oprd1 = new Simbolo(id4.getLexico(), id4.getToken(), id4.getTipo());
                    pilha.push(oprd1);
                } else {
                    System.out.println("\n------------------------\nERRO SEMÂNTICO\nLinha: " + linha + " - Erro identificado na análise semântica.");
                    System.out.println("Erro: Variável não declarada.\n------------------------");
                    System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                    System.exit(0);
                }
                break;
            case 21:
                Simbolo num1 = pilha.pop();
                Simbolo oprd2 = new Simbolo(num1.getLexico(), num1.getToken(), num1.getTipo());
                pilha.push(oprd2);
                break;
            case 22:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("A", null, null));
                break;
            case 23:
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("COND", null, null));
                imprimir("}");
                break;
            case 24:
                Simbolo entao = pilha.pop();
                pilha.pop();
                Simbolo expr = pilha.pop();
                pilha.pop();
                Simbolo se = pilha.pop();
                pilha.push(new Simbolo("CABECALHO", null, null));
                imprimir("if (" + expr.getLexico() + "){");
                break;
            case 25:
                Simbolo oprdFinal1 = pilha.pop();
                Simbolo opr = pilha.pop();
                Simbolo oprdInicial1 = pilha.pop();
                if (oprdFinal1.getTipo().equals(oprdInicial1.getTipo())) {
                    Simbolo expr1 = new Simbolo("T" + contador, null, oprdFinal1.getTipo());
                    cabecalhoArquivo = cabecalhoArquivo + oprdFinal1.getTipo() + " T" + contador + ";\n";
                    contador++;
                    imprimir(expr1.getLexico() + " = " + oprdInicial1.getLexico() + " " + opr.getTipo() + " " + oprdFinal1.getLexico() + ";");
                    pilha.push(expr1);
                } else {
                    System.out.println("\n------------------------\nERRO SEMÂNTICO\nLinha: " + linha + " - Erro identificado na análise semântica.");
                    System.out.println("Erro: Operandos com tipos incompatíveis.\n------------------------");
                    System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                    System.exit(0);
                }
                break;
            case 26:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("CORPO", null, null));
                break;
            case 27:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("CORPO", null, null));
                break;
            case 28:
                //Nada.
                pilha.pop();
                pilha.pop();
                pilha.push(new Simbolo("CORPO", null, null));
                break;
            case 29:
                //Nada.
                pilha.pop();
                pilha.push(new Simbolo("CORPO", null, null));
                break;
            case 30:
                //Nada.
                pilha.pop();
                pilha.push(new Simbolo("A", null, null));
                break;
        }
    }

    /**
     * Metodo que imprimi comandos no arquivo final.
     *
     * @param comando
     */
    private void imprimir(String comando) throws IOException {
        System.out.println("IMPRIMIR NO ARQUIVO: " + comando + "\n");
        textoArquivo = textoArquivo + comando + "\n";
    }

    /**
     * Metodo que finaliza o arquivo.
     *
     * @throws IOException
     */
    public void finalizarArquivo() throws IOException {
        imprimir("}");
        cabecalhoArquivo = cabecalhoArquivo + "/*------------------------------*/\n";
        writer.write(cabecalhoArquivo);
        writer.write(textoArquivo);
        arquivo.close();
    }

    /**
     * Metodo que altera tipo do simbolo no topo da pilha.
     *
     * @param tipo
     */
    public void alteraTipoAnterior(String tipo) {
        pilha.peek().setTipo(tipo);
    }

    /**
     * Metodo que mostra pilha semnatica.
     */
    public void mostrarPilha() {
        for (Simbolo s : pilha) {
            System.out.println(s);
        }
    }

}
