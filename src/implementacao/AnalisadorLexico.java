/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacao;

import model.TabelaDeToken;
import model.TabelaDeSimbolos;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import model.Simbolo;

/**
 *
 * @author Matheus
 */
public class AnalisadorLexico {

    private File file;
    FileReader reader;
    BufferedReader bF;
    private String controle;
    private int contLinha;
    private int contColunaLinha;
    private int contColuna;
    private TabelaDeSimbolos tabelaSimbolos;
    private TabelaDeToken tabelaToken;
    private char caracter;
    private String palavraAtual;
    private int estado;
    private boolean EOF;

    /**
     * Metodo construtor da classe.
     *
     * @param file
     * @throws FileNotFoundException
     */
    public AnalisadorLexico(File file) throws FileNotFoundException {
        this.contLinha = 0;
        this.contColunaLinha = 0;
        this.contColuna = 0;
        this.file = file;
        this.reader = new FileReader(file);
        this.bF = new BufferedReader(reader);
        this.tabelaSimbolos = new TabelaDeSimbolos();
        this.tabelaToken = new TabelaDeToken();
        this.palavraAtual = "";
        this.estado = 0;
        this.EOF = false;
    }

    /**
     * Metodo que ira fazer a analise lexica retornando o proximo simbolo.
     *
     * @return
     */
    public Simbolo retornaSimbolo() throws IOException {

        do {

            caracter = (char) bF.read();
            String c = retornaValorParaPesquisar(caracter);

            // Contador de linhas.
            if (c == "Salto" && contColunaLinha != 0) {
                contLinha++;
                contColunaLinha = 0;
            } else {
                contColunaLinha++;
            }

            // Contador de colunas.
            if (c == "Salto") {
                contColuna = 0;
            } else {
                contColuna++;
            }

            // Final do arquivo.
            if (c.equals("EOF")) {
                EOF = true;
            }

            int estadoAnterior = estado;
            estado = tabelaToken.obterProximoEstado(estado, c);

            switch (estado) {

                // Caso encontre alguma intercorrencia.
                case -1:

                    // Verifica se o estado e final.
                    if (estadoAnterior != -1 && tabelaToken.eFinal(estadoAnterior)) {

                        Simbolo simbolo = tabelaSimbolos.obterSimbolo(palavraAtual);

                        // Insere um novo simbolo caso o mesmo nao seja encontrado.
                        if (simbolo == null) {
                            String token = tabelaToken.obterToken(estadoAnterior);
                            simbolo = new Simbolo(palavraAtual, token, null);
                            if (token.equals("id")) {
                                tabelaSimbolos.inserirSimbolo(palavraAtual, simbolo);
                            } else if (token.equals("opm")) {
                                simbolo.setTipo(simbolo.getLexico());
                            } else if (token.equals("opr")) {
                                simbolo.setTipo(simbolo.getLexico());
                            } else if (token.equals("rcb")) {
                                simbolo.setTipo("=");
                            } else if (token.equals("num")) {
                                if (!palavraAtual.contains(".")) {
                                    simbolo.setTipo("int");
                                } else {
                                    simbolo.setTipo("double");
                                }
                            }
                        }

                        if ((!c.equals("Salto")) && (!c.equals("Tab")) && (!c.equals("Esp"))) {
                            // Ja inicia a palavra.
                            palavraAtual = "" + caracter;
                            estado = tabelaToken.obterProximoEstado(0, retornaValorParaPesquisar(caracter));
                        } else {
                            // Volta ao inicio do automato.
                            palavraAtual = "";
                            estado = 0;
                        }

                        if (simbolo.getToken().equals("comentario")) {
                            return retornaSimbolo();
                        } else {
                            return simbolo;
                        }

                    } else {
                        System.out.println("\n------------------------\nERRO LÉXICO \nLinha: " + (contLinha + 1) + "\nColuna: " + (contColuna - 1) + "\nO sistema não esperava o caractere: " + palavraAtual + "\n------------------------");
                        System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                        System.exit(0);
                    }

                    break;

                //Ignorar espacos e etc.
                case 0:
                    break;

                //Caso percorra o afd normalmente.    
                default:
                    palavraAtual = palavraAtual + caracter;
                    break;
            }

        } while (!EOF);

        return new Simbolo("EOF", "EOF", null);

    }

    /**
     * Metodo que passara o caracter para um valor que possa ser pesquisado no
     * automato.
     *
     * @param c
     * @return
     */
    private String retornaValorParaPesquisar(char c) {

        String caractere = "" + c;

        if (controle == null) {
            if (caractere.equals("\n") || caractere.equals("\r")) {
                return "Salto";
            } else if (caractere.equals("\t")) {
                return "Tab";
            } else if (caractere.equals(" ")) {
                return "Esp";

            } else if (c == 65535) {
                return "EOF";

            } else if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z') {
                return "L";
            } else if (c >= '0' && c <= '9') {
                return "D";
            } else {
                if (c == '\"') {
                    controle = "\"";
                }
                if (c == '{') {
                    controle = "{";
                }
                return "" + c;
            }
        } else if (controle.equals("\"") && c == '\"') {
            controle = null;
            return "" + c;
        } else if (controle.equals("{") && c == '}') {
            controle = null;
            return "" + c;
        } else {
            return ".";
        }

    }

    /**
     * Metodo que retorna a tabela de simbolos.
     *
     * @return
     */
    public TabelaDeSimbolos retornaTabelaSimbolos() {
        return this.tabelaSimbolos;
    }

    public int getContLinha() {
        return contLinha;
    }

    public int getContColuna() {
        return contColuna;
    }

}
