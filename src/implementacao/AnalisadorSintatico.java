/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;
import model.ErrosSintaticos;
import model.ProducaoGramatica;
import model.Simbolo;
import model.TabelaDeSintaxe;

/**
 *
 * @author Matheus
 */
public class AnalisadorSintatico {

    private HashMap<String, HashMap<String, String>> tabelaSintaxe = new HashMap<>();
    private AnalisadorSemantico analisadorSemantico;

    /**
     * Metodo construtor da classe.
     *
     * @param pathTabelaSintatica
     * @param pathProgramaCompilado
     * @throws java.io.IOException
     */
    public AnalisadorSintatico(String pathTabelaSintatica, String pathProgramaCompilado) throws IOException {

        TabelaDeSintaxe tS = new TabelaDeSintaxe();
        this.tabelaSintaxe = tS.lerTabela(pathTabelaSintatica);
        this.analisadorSemantico = new AnalisadorSemantico(pathProgramaCompilado);
    }

    /**
     * Metodo que ira fazer a analise sintatica do programa mgol.
     *
     * @param file
     */
    public void analisar(File file) throws FileNotFoundException, IOException {

        AnalisadorLexico analiseLexica = new AnalisadorLexico(file);
        ProducaoGramatica pG = new ProducaoGramatica();
        Stack<String> pilha = new Stack<>();
        pilha.push("0");
        Simbolo palavra = analiseLexica.retornaSimbolo();

        while (true) {

            if (palavra.equals("EOF")) {
                System.exit(0);
            }

            String acao = obterProximoEstado(pilha.peek(), palavra.getToken());

            // Empilha
            if (acao.substring(0, 1).equals("S")) {
                //System.out.println(pilha.peek() + " - " + palavra.getLexico() + " - " + palavra.getToken() + " - " + acao + "\n");
                pilha.push(acao.substring(1));
                // Adicionando tipo de dados caso necessário.
                if (palavra.getToken().equals("lit")) {
                    analisadorSemantico.alteraTipoAnterior("literal");
                } else if (palavra.getToken().equals("int")) {
                    analisadorSemantico.alteraTipoAnterior("int");
                } else if (palavra.getToken().equals("real")) {
                    analisadorSemantico.alteraTipoAnterior("double");
                }
                analisadorSemantico.adicionarPilha(palavra);
                palavra = analiseLexica.retornaSimbolo();

                // Reduz    
            } else if (acao.substring(0, 1).equals("R")) {
                //System.out.println(pilha.peek() + " - " + palavra.getLexico() + " - " + palavra.getToken() + " - " + acao);

                int qtdEstados = pG.obterQuantidadeCasas(acao.substring(1));
                for (int i = 0; i < qtdEstados; i++) {
                    pilha.pop();
                }

                // Realizando desvio.
                String estado = obterProximoEstado(pilha.peek(), pG.obterProdutor(acao.substring(1)));
                pilha.push(estado);

                System.out.println(acao + " - " + pG.obterProducao(acao.substring(1)));
                analisadorSemantico.analisar(analiseLexica.retornaTabelaSimbolos(), Integer.parseInt(acao.substring(1)), analiseLexica.getContLinha());

                // Aceita    
            } else if (acao.substring(0, 1).equals("A")) {
                analisadorSemantico.finalizarArquivo();
                System.out.println("\n------------------------\nPROGRAMA ACEITO\n------------------------");
                break;

                // Erro
            } else {
                System.out.println("\n------------------------\nERRO SINTATICO");
                ErrosSintaticos erro = new ErrosSintaticos();
                String erroSintatico = erro.obterErro(acao.substring(1));
                System.out.println(erroSintatico);
                String erroLocal = "Linha: " + analiseLexica.getContLinha() + "\nColuna: " + analiseLexica.getContColuna() + "\n------------------------";
                System.out.println(erroLocal);
                System.out.println("\n------------------------\nPROGRAMA NÃO ACEITO\n------------------------");
                break;
            }
        }
    }

    /**
     * Metodo responsavel por retorna o proximo estado.
     *
     * @param estado
     * @param simbolo
     * @return
     */
    public String obterProximoEstado(String estado, String simbolo) {
        return tabelaSintaxe.get(estado).get(simbolo);
    }

}
