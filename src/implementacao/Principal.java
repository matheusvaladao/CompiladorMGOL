/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacao;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author Matheus
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        File file = new File("src\\arquivosTeste\\programaMgol.mgol");
        String pathTabelaSintatica = "src\\arquivosAFD\\tabelaAFDSintaxeErro.csv";
        String pathProgramaCompilado = "C:\\Users\\Matheus\\Documents\\ProgramaCompilador.c";
        AnalisadorSintatico analiseSintatica = new AnalisadorSintatico(pathTabelaSintatica, pathProgramaCompilado);
        analiseSintatica.analisar(file);
    }
}
